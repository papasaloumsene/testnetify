import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ContactService } from '../contact.service';
import { IContact } from '../model/IContact';
import { IGroup } from '../model/IGroup';

@Component({
  selector: 'app-addcontact',
  templateUrl: './addcontact.component.html',
  styleUrls: ['./addcontact.component.css']
})
export class AddcontactComponent implements OnInit {

  public groups: IGroup[]  = [] as IGroup[];
  public contact: IContact = {} as IContact;
  public loading: boolean = false;
  public contactId: string | null = null;
  public errorMessage:string | null = null

  constructor(private contactService: ContactService
                        ,private router: Router) { }

  ngOnInit(): void {
    this.contactService.getAllGroups().subscribe((data)=>{
      this.groups= data
    },(error)=>{
      this.errorMessage = error; 
      
    })
  }

 public createSubmit(){
  this.contactService.createContact(this.contact).subscribe((data)=>{
    this.router.navigate(['/']).then(); 
  },(error)=>{
    this.errorMessage = error;
    this.router.navigate(['/contacts/add']).then();
    
  })
 }


}
