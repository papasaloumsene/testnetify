import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContactMgrComponent } from './contact-mgr/contact-mgr.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ViewcontactComponent } from './viewcontact/viewcontact.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule} from '@angular/common';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';
import { AddcontactComponent } from './addcontact/addcontact.component';
import { EditcontactComponent } from './editcontact/editcontact.component';

@NgModule({
  declarations: [
    AppComponent,
    ContactMgrComponent,
    NavbarComponent,
    AddcontactComponent,
    EditcontactComponent,
    ViewcontactComponent,
    SpinnerComponent,
    PageNotFoundComponent,
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    CommonModule,
    FormsModule
 
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
