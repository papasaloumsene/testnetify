import { Component, OnInit } from '@angular/core';
import { ContactService } from '../contact.service';
import { IContact } from '../model/IContact';

@Component({
  selector: 'app-contact-mgr',
  templateUrl: './contact-mgr.component.html',
  styleUrls: ['./contact-mgr.component.css']
})
export class ContactMgrComponent implements OnInit {

  public loading: boolean = false;
  public contacts:IContact[] = [];
  public errorMessage: null | String= null;  
  constructor(private contactService: ContactService) { }

  ngOnInit(): void {
    this.getContactFromServer();
  }
 public getContactFromServer(){
  this.loading = true
  this.contactService.getAllContact().subscribe( (data)=>{
    this.contacts= data;
    this.loading = false; 
  },
    (error)=>{
      this.errorMessage = error;
      this.loading = false; 
    }
  );
 }

 public  deleteContact(contactId: string | undefined){
  if(contactId){
    this.contactService.deleteContact(contactId).subscribe(()=>{
      this.getContactFromServer();
    },(error)=>{
      this.errorMessage= error;
    })
  }
 }
}
