import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { catchError, Observable, throwError } from "rxjs";
import { IContact } from "./model/IContact";
import { IGroup } from "./model/IGroup";


@Injectable({
  providedIn: 'root',
})
export class ContactService {

  constructor(private httpClient: HttpClient){}

  private serverUrl:string = `http://localhost:9000`

  public getAllContact(): Observable<IContact[]>{

    let dataUrl:string = `${this.serverUrl}/contacts`
    return this.httpClient.get<IContact[]>(dataUrl).pipe(catchError(this.handleError))
  }

  public getSingleContact(contactId: String):Observable<IContact>{

    let dataUrl:string = `${this.serverUrl}/contacts/${contactId}`
    return this.httpClient.get<IContact>(dataUrl).pipe(catchError(this.handleError))
  }
  public createContact(contact: IContact):Observable<IContact>{

    let dataUrl:string = `${this.serverUrl}/contacts`
    return this.httpClient.post<IContact>(dataUrl, contact).pipe(catchError(this.handleError))
  }

  public updateContact(contact: IContact, contactId: string):Observable<IContact>{

    let dataUrl:string = `${this.serverUrl}/contacts/${contactId}`;
    return this.httpClient.put<IContact>(dataUrl, contact).pipe(catchError(this.handleError));
  }

  public deleteContact(contactId: string):Observable<{}>{

    let dataUrl:string = `${this.serverUrl}/contacts/${contactId}`
    return this.httpClient.delete<{}>(dataUrl).pipe(catchError(this.handleError))
  }
  public getAllGroups():Observable<IGroup[]>{

    let dataUrl:string = `${this.serverUrl}/groups`
    return this.httpClient.get<IGroup[]>(dataUrl).pipe(catchError(this.handleError))
  }
  public getSingleGroup():Observable<IGroup>{

    let dataUrl:string = `${this.serverUrl}/groups`
    return this.httpClient.get<IGroup>(dataUrl).pipe(catchError(this.handleError))
  }
  handleError(error: HttpErrorResponse) {
    
    let errorMessage:string = '';
    if(error.error instanceof ErrorEvent){

      errorMessage = `error: ${error.error.message}`
    }else{
      errorMessage = ` Status: ${error.status} \n message: ${error.message}`
    }
    return throwError(errorMessage);
  }
  
}

