import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, TitleStrategy } from '@angular/router';
import { ContactService } from '../contact.service';
import { IContact } from '../model/IContact';
import { IGroup } from '../model/IGroup';

@Component({
  selector: 'app-editcontact',
  templateUrl: './editcontact.component.html',
  styleUrls: ['./editcontact.component.css']
})
export class EditcontactComponent implements OnInit {

  public contact:IContact = {} as IContact;
  public contactId:string | null= null;
  public groups: IGroup[]= [] as IGroup[];
  public loading: Boolean = false;
  public errorMessage: string | null= null;

 constructor(private activetedRoute: ActivatedRoute, 
              private contactService: ContactService, 
              private router:Router){}
  
  ngOnInit(): void {
    this.loading=true
   this.activetedRoute.paramMap.subscribe((param)=>{
    this.contactId = param.get('contactId')
   });
   
   if(this.contactId){
    this.contactService.getSingleContact(this.contactId).subscribe((data)=>{
      this.contact =data;
      this.contactService.getAllGroups().subscribe((data)=>{
        this.loading=false
      this.groups =data;
      })
      
    },(error)=>{
      this.errorMessage=error
      this.loading= false
    })
   }
  }

public updateContact(){ 
  if(this.contactId){
    this.contactService.updateContact(this.contact,this.contactId).subscribe((data)=>{
      this.router.navigate(['/']).then();
    },(error)=>{
      this.errorMessage=error;
      this.router.navigate([`contact/edit/${this.contactId}`]).then()
    });
  }
 }
}
