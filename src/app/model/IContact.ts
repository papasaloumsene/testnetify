export interface IContact{
    id:string;
    name:string;
    mobile:string;
    email:string;
    photo:string;
    title:string;
    company:string;
    groupId:string;
}