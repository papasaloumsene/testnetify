import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, TitleStrategy } from '@angular/router';
import { ContactService } from '../contact.service';
import { IContact } from '../model/IContact';
import { IGroup } from '../model/IGroup';


@Component({
  selector: 'app-viewcontact',
  templateUrl: './viewcontact.component.html',
  styleUrls: ['./viewcontact.component.css']
})
export class ViewcontactComponent implements OnInit {


public contact: IContact = {} as IContact ;
public loading:boolean = false;
public errorMessage: string | null = null;
public contactId:string | null  = null;
public groups:IGroup = {} as IGroup;

 constructor(private activatedRoute: ActivatedRoute,
                    private  contactService: ContactService){} 
 
 ngOnInit(): void {
  this.loading= true;
  this.activatedRoute.paramMap.subscribe((param)=>{
    this.contactId = param.get('contactId')
    
  });

  if(this.contactId){
    this.contactService.getSingleContact(this.contactId).subscribe((data)=>{
      this.contact = data;
      this.contactService.getSingleGroup().subscribe((data)=>{
        this.groups=data;
        this.loading= false;
      })
    },(error)=>{
      this.errorMessage=error;
      this.loading=false
    })
  }

}
public isNotEmpty(){
  return Object.keys(this.contact).length > 0 && Object.keys(this.groups).length > 0;
}
}